# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=foot
pkgver=1.17.1
pkgrel=0
pkgdesc="Fast, lightweight and minimalistic Wayland terminal emulator"
url="https://codeberg.org/dnkl/foot"
license="MIT"
arch="all"
depends="ncurses-terminfo"
makedepends="
	cage
	font-dejavu
	fcft-dev
	fontconfig-dev
	freetype-dev
	libxkbcommon-dev
	meson
	ncurses
	pixman-dev
	scdoc
	tllist-dev
	utf8proc-dev
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-extra-terminfo:_extra_terminfo:noarch
	$pkgname-themes:_themes:noarch
	"
source="
	$pkgname-$pkgver.tar.gz::https://codeberg.org/dnkl/foot/archive/$pkgver.tar.gz
	"
options="!check" # ran during profiling
builddir="$srcdir/foot"

build() {
	export CFLAGS="$CFLAGS -O3" # -O3 as the package is intended to use it
	export CXXFLAGS="$CXXFLAGS -O3"
	export CPPFLAGS="$CPPFLAGS -O3"

	abuild-meson \
		-Db_pgo=generate \
		-Db_lto=true \
		-Dterminfo-base-name=foot-extra \
		-Dutmp-backend=none \
		. output
	meson compile -C output

	ninja -C output test
	./pgo/full-headless-cage.sh . output

	meson configure -Db_pgo=use output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_extra_terminfo() {
	pkgdesc="$pkgdesc (extra terminfo data)"

	amove /usr/share/terminfo/f
}

_themes() {
	pkgdesc="$pkgdesc (color schemes)"

	amove /usr/share/foot/themes
}

sha512sums="
cfc9b074e4c0ec5f7deb50f3fe7b192cf4218b087b1b77774a8b39bbe7959a8de6a03e643446fac25d857a73e16fc1233f0c4da7160af820af7a9f63dea5d487  foot-1.17.1.tar.gz
"

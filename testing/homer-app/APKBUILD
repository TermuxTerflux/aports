# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=homer-app
pkgver=1.4.59
pkgrel=2
pkgdesc="HOMER 7.x Front-End and API Server"
url="http://sipcapture.io"
arch="x86_64"
license="AGPL-3.0-or-later"
makedepends="go"
install="$pkgname.pre-install"
options="!check" # no test suite
subpackages="$pkgname-doc $pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sipcapture/homer-app/archive/refs/tags/$pkgver.tar.gz
	https://github.com/sipcapture/homer-app/releases/download/$pkgver/homer-ui-$pkgver.tgz
	$pkgname.initd

	0001-change-default-paths.patch
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make modules
	make all
}

check() {
	"release/homer-app_linux_amd64/homer-app" --version
}

package() {
	install -D -m755 release/homer-app_linux_amd64/homer-app "$pkgdir"/usr/bin/homer-app
	install -D -m644 etc/webapp_config.json "$pkgdir"/etc/homer/webapp_config.json
	mkdir -p "$pkgdir"/usr/share/webapps/homer \
		"$pkgdir"/var/log/homer \
		"$pkgdir"/usr/share/licenses/$pkgname
	mv "$srcdir"/dist/3rdpartylicenses.txt \
		"$pkgdir"/usr/share/licenses/$pkgname/3rdpartylicenses.txt
	mv "$srcdir"/dist "$pkgdir"/usr/share/webapps/homer
	install -D "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}

sha512sums="
b62ded15ff9ad2651b4e58ecf497b10208cc0a9075eca01a62328c71402c14a7a12b5903a4ea1ce86d38f38052fbbcdda3f498f120fee77a2fd271b87a41fe3c  homer-app-1.4.59.tar.gz
561f858f72a620abe041d0bff8632e57c25fcfa5f13053fa416471deb4d9d766a4c90602b7a6b0987165d2ba61ccbcf66a567e53a99be63c704b341350258c87  homer-ui-1.4.59.tgz
0eaace71fe329bead88d7180aa5ad80f6924f1e0b3426cfa45811121ff4e907b32a4b08d6db995ae77b9b9ddbdbfc66411c03c2eea9c0393f6b9ed12ad851a77  homer-app.initd
628b335abc0dab38fb43c95fafb10ad164567d639dc39504c3947c7d865163cb5b3036255bc7d898c5a1d3db2bb5e1d7b8695c790a29d7ba68c5961a05295553  0001-change-default-paths.patch
"

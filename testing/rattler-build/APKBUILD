# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rattler-build
pkgver=0.14.2
pkgrel=0
pkgdesc="A fast conda-package builder"
url="https://github.com/prefix-dev/rattler-build"
arch="all"
license="BSD-3-Clause"
depends="
	bzip2
	xz
	"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
checkdepends="
	patchelf
	git
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/prefix-dev/rattler-build/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
	mkdir -p completions/
}

build() {
	cargo auditable build --frozen --release
	local _completion="target/release/$pkgname completion"
	$_completion --shell bash > "completions/$pkgname"
	$_completion --shell fish > "completions/$pkgname.fish"
	$_completion --shell zsh  > "completions/_$pkgname"
}

check() {
	cargo test --frozen -- --skip "test_host_git_source"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 664 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 664 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 664 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
5e6b023fd1ea8a6b4f3b2858b4a24fe9c60ba1b36e8cced5bd4990db13a69461cf4822a02acff73d4ac1381575db2d22c1128ac1f62a00befa57a170a0bc7891  rattler-build-0.14.2.tar.gz
"
